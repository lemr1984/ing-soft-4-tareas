<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title>Ejercicio5</title>
  </head>
  <body>
    <?php
    	$conn = new PDO('pgsql:host=localhost;dbname=ejercitario5;', 'postgres', 'postgres');
		$sqlProductos = 'select p.producto_id producto_id, p.nombre producto_nombre, p.descripcion producto_descripcion, m.nombre producto_marca, t.nombre producto_tipo
					from producto p
					join marca m on p.marca_id = m.marca_id 
					join tipo t on p.tipo_id = t.tipo_id
					order by 2;';
		$sqlMarca = 'select * from marca';
		$sqlTipo = 'select * from tipo';

		echo'
			<div class = "container">
				<form role="form" method="POST" action="guardar-datos.php">
					<fieldset>
					<legend>Datos del producto a ser ingresado</legend>
						<input type="hidden" name="codigo">
						<input type="text" name="nombreProducto" placeholder="Nombre del Producto" required autofocus>
						<br><br>
						<input type = "text" name = "descProducto" placeholder = "Descripción" required autofocus>
						<br>
						<p>Seleccione una marca</p>
						<select name="marcaProducto" id="idMarca">';
						foreach ($conn->query($sqlMarca) as $array1)
							{ 
								echo '<option value="'. $array1['marca_id'].'">'. $array1['nombre'].'</option>';
							}
						echo '</select>
						<br>
						<p>Seleccione un tipo</p>
						<select name="tipoProducto" id="idTipo">';
						foreach ($conn->query($sqlTipo) as $array2)
							{
								echo '<option value="'. $array2['tipo_id'].'">'. $array2['nombre'].'</option>';
							}
						echo '</select>
						<br><br>
						<button type = "submit" name = "guardarDatos">Guardar</button>
						<br>
					</fieldset>
	            </form>
            </div> 
			
			<h3>Productos actuales</h3>
			
			<table>
				<tr>
				<th>Codigo</th>
				<th>Producto</th>
				<th>Descripción</th>
				<th>Marca</th>
				<th>Tipo</th>
				<th>Operación</th>
				<th>Operación</th>
				</tr>
				';
				foreach ($conn->query($sqlProductos) as $array3) {
					echo'<tr>
							<td>'. $array3['producto_id'].'</td>
							<td>'. $array3['producto_nombre'].'</td>
							<td>'. $array3['producto_descripcion'].'</td>
							<td>'. $array3['producto_marca'].'</td>
							<td>'. $array3['producto_tipo'].'</td>
							<td>  
							<form method="post" action="eliminar-datos.php"> 
								<input type="submit" name="delete" value="Borrar 🗑️" >
								<input type="hidden" name="idDelete" value="'.$array3['producto_id'].'">
							</form>
							</td>
							<td><a class="btn btn-warning" href=obtener-datos.php?idEdit="'.$array3['producto_id'].'">Editar 📝</a></td>
						</tr>';
				}
		    echo'</table>
				<style>
				table,th, td 
				{
				border: 1px solid black;
				border-collapse: collapse;
				padding: 5px;
				}			
				</style>
				';
    ?>
  </body>
</html>