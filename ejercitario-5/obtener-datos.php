<?php
    
    $conn_string = "host=localhost port=5432 dbname=ejercitario5 user=postgres password=postgres";
	$conn = pg_connect($conn_string);
    
    if(count($_POST)>0) {
      pg_query($conn,"UPDATE producto set tipo_id='" . $_POST['tipoId'] . "', marca_id='" . $_POST['marcaId'] . "', nombre='" . $_POST['nombre'] . "', descripcion='" . $_POST['descripcion'] . "' WHERE producto_id='" . $_POST['idEdit'] . "'");
	}
	
    $id = $_GET['idEdit'];
	$idInt = trim($id, "'\"");

    $sql = "select * from producto where producto_id = '{$idInt}'";

	$datos = pg_query($conn, $sql);
	$row = pg_fetch_array($datos);
	
	$sqlMarca = 'select * from marca';
	$sqlTipo = 'select * from tipo';

	$marcas_query = pg_query($conn, $sqlMarca);
	$tipos_query = pg_query($conn, $sqlTipo);

	$marcas=pg_fetch_all($marcas_query);
	$tipos=pg_fetch_all($tipos_query);

?>
	<html>
      <head>
        <title>Update Data</title>
      </head>

      <body>
        <form name="frm" method="post" action="editar-datos.php">
        <div><?php if(isset($message)) { echo $message; } ?>
        </div>
        <div style="padding-bottom:5px;">
        <a href="mantener-producto.php">Volver al listado de productos</a>
        </div>
        <input type="hidden" name="idEdit" class="txtField" value="<?php echo $row['producto_id']; ?>">
		<p>Seleccione una marca</p>
		<select name="marcaId" id="idMarca">';
		<?php foreach ($marcas as $array1)
			{ 
				echo '<option value="'.$array1['marca_id'].'">'.$array1['nombre'].'</option>';
			}
		?>
		echo '</select>
		<br>
		<p>Seleccione un tipo</p>
		<select name="tipoId" id="idTipo">';
		<?php foreach ($tipos as $array2)
			{
				echo '<option value="'.$array2['tipo_id'].'">'.$array2['nombre'].'</option>';
			}
		?>
		</select>
        <br><br>
        Nombre:<br>
        <input type="text" name="nombre" class="txtField" value="<?php echo $row['nombre']; ?>">
        <br><br>
        Descripción:<br>
        <input type="text" name="descripcion" class="txtField" value="<?php echo $row['descripcion']; ?>">
        <br><br>
        <input type="submit" name="submit" value="Guardar cambios" class="buttom">
        </form>
      </body>
	</html>
