<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title>Ejercicio4</title>
  </head>
  <body>
    <?php
          $conn = new PDO('pgsql:host=localhost;dbname=ejercicio1;', 'postgres', 'postgres');
		  $sql = 'select p.nombre nombre_producto, p.precio precio_producto, m.nombre nombre_marca, e.nombre nombre_empresa, c.nombre nombre_categoria 
							from productos p
							join marcas m on p.id_marca = m.id_marca 
							join categorias c on p.id_categoria = c.id_categoria 
							join empresas e on m.id_empresa = e.id_empresa 
							order by 1';
		echo'
				<table>
				  <tr>
					<th>Producto</th>
					<th>Precio</th>
					<th>Marca</th>
					<th>Empresa</th>
					<th>Categoria</th>
				  </tr>
		';
		 foreach ($conn->query($sql) as $array) {
			echo '<tr>
				<td>'. $array['nombre_producto'].'</td>
				<td>'. $array['precio_producto'].'</td>
				<td>'. $array['nombre_marca'].'</td>
				<td>'. $array['nombre_empresa'].'</td>
				<td>'. $array['nombre_categoria'].'</td>
			  </tr>';
			}
		echo '</table>
			<style>
			table,th, td {
			  border: 1px solid black;
			  border-collapse: collapse;
			  padding: 5px;
			}			
			</style>
		';
    ?>
  </body>
</html>