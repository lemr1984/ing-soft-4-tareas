<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<title>Ejercicio3</title>
	</head>
	<body>
		<?php
		    //CREAMOS LA CONEXIÓN CON EL SERVIDOR QUE SE ALMACENARÁ EN $conexion
		    $conn_string = "host=localhost port=5432 dbname=ejercicio1 user=postgres password=postgres";
			$conexion = pg_connect($conn_string);

		    $sql = "select p.nombre nombre_producto, p.precio precio_producto, m.nombre nombre_marca, e.nombre nombre_empresa, c.nombre nombre_categoria 
					from productos p
					join marcas m on p.id_marca = m.id_marca 
					join categorias c on p.id_categoria = c.id_categoria 
					join empresas e on m.id_empresa = e.id_empresa 
					order by 1";

		    $datos = pg_query($conexion, $sql);
		    $res = pg_fetch_all($datos);
			echo '
				<table>
				  <tr>
					<th>Producto</th>
					<th>Precio</th>
					<th>Marca</th>
					<th>Empresa</th>
					<th>Categoria</th>
				  </tr>
				  ';
			
				foreach($res as $array)
				{
					echo '<tr>
							<td>'. $array['nombre_producto'].'</td>
							<td>'. $array['precio_producto'].'</td>
							<td>'. $array['nombre_marca'].'</td>
							<td>'. $array['nombre_empresa'].'</td>
							<td>'. $array['nombre_categoria'].'</td>
						  </tr>';
				}
			echo '</table>
			<style>
			table,th, td {
			  border: 1px solid black;
			  border-collapse: collapse;
			  padding: 5px;
			}			
			</style>
			';
		?>
	</body>
</html>