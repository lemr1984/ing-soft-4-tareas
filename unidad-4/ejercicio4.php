<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<title>Unidad-Ejercicio4</title>
	</head>
	<body>
		<?php
			$var = 24.5;
			echo "Variable: ".' '.$var;

			echo "<br>";

			echo "Tipo de dato de la variable: ".' '.gettype($var), "\n";

			echo "<br>";

			$var = "HOLA";

			echo "<br>";

			echo "Variable: ".' '.$var;

			echo "<br>";

			echo "Tipo de dato de la variable: ";

			echo gettype($var), "\n";

			echo "<br>";

			settype($var, 'integer');

			echo "<br>"; 

			echo "var_dump: ";
			var_dump($var);


		?>
	</body>
</html>
