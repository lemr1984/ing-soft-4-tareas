<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title>Unidad5-Ejercicio17</title>
  </head>
  <body>
    <div class="cuerpo">
      <?php
          /*  Representar una estructura de árbol n-ario utilizando arrays de PHP. Representar un árbol similar
              al de la figura.
                                   R
                              /    l       \
                            S      T          D
                        /  \      /  \       /   \
                      U     V    G     F    H     J
                /  l  \        / \         / \    / \
              L    Q   W      C   K       A  X   Z   I   */


      $hijos_U["L"]=" ";
      $hijos_U["Q"]=" ";
      $hijos_U["W"]=" ";
      $hijos_G["C"]=" ";
      $hijos_G["K"]=" ";
      $hijos_H["A"]=" ";
      $hijos_H["X"]=" ";
      $hijos_J["Z"]=" ";
      $hijos_J["I"]=" ";
      $hijos_S["U"]=$hijos_U;
      $hijos_S["V"]=" ";
      $hijos_T["G"]=$hijos_G;
      $hijos_T["F"]="";
      $hijos_D["H"]=$hijos_H;
      $hijos_D["J"]=$hijos_J;
      $hijos_R["S"]=$hijos_S;
      $hijos_R["T"]=$hijos_T;
      $hijos_R["D"]=$hijos_D;
      $arbol_n_ario['R']= $hijos_R;

      print_r($arbol_n_ario);
      echo "<br/>";
      echo "<br/>";
      foreach ($arbol_n_ario as $key => $value) {
        echo "<br/>".$key;
        while (is_array($value)==true) {
          $aux=$value;
          foreach ($aux as $key => $value) {
            echo "<br/> -".$key;
            while (is_array($value)==true) {
              $aux1=$value;
              foreach ($aux1 as $key => $value) {
                echo "<br/> --------".$key."";
                while (is_array($value)==true) {
                  $aux2=$value;
                  foreach ($aux2 as $key => $value) {
                    echo "<br/> ----------------".$key."";
                    while (is_array($value)==true) {
                      $aux3=$value;
                      foreach ($aux3 as $key => $value) {
                        echo "<br/> -------------------------".$key."";
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      ?>
    </div>
  </body>
</html>
