<?php
/* Se debe recorrer dicho array e imprimir sólo las claves del array que empiecen con la letra
a, d, m y z (función imprimir) (en el caso se no existir ningún índice que comience con esas
letras se debe imprimir un mensaje).
Observación: Se debe crear un archivo por función y el archivo principal donde se llaman a las
funciones y se realizan los procesamientos. */

function imprimir_buscados($v){
  $encontre = false;
  foreach ($v as $key => $value) {
    if ((strpos( $key, "a" ) === 0) or (strpos( $key, "d" ) === 0) or (strpos( $key, "m" ) === 0) or (strpos( $key, "z" ) === 0) ) {
      echo "- ".$key."<br />";
      $encontre = true;
    }
  }

  if (!$encontre){
      echo "No existen resultados";
  }
}
?>