<?php
/*Crear un array asociativo en PHP que cumpla los siguientes requisitos:
  • Los índices/claves deben ser strings (estas cadenas deben ser generadas aleatoriamente
  por el script y deben tener una longitud de 5 a 10 caracteres).
  • Los valores deben ser números enteros del 1 a 1000. */

function generate_string($input, $long) {
    $input_long = strlen($input);
    $random_string = '';
    for($i = 0; $i < $long; $i++) {
        $random_character = $input[mt_rand(0, $input_long - 1)];
        $random_string .= $random_character;
    }
    return $random_string;
}

function generar_array(){
  $caracteres_permitidos = 'abcdefghijklmnopqrstuvwxyz';
  mt_srand( time());
  $arrayito = array();
  for ($i=0; $i < 10; $i++) {
    $longitud= mt_rand(5,10);
    $indice=generate_string($caracteres_permitidos, $longitud);
    $arrayito[$indice]= mt_rand(1,1000);
  }
  return $arrayito;
}
?>