<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title>Unidad5-Ejercicio15</title>
  </head>

  <body>
  <div class="cuerpo">
  <?php
    /*  Declarar un vector de 20 elementos con valores aleatorios de 1 a 10.
    Crear una función recursiva en PHP que recorra el array de atrás para adelante. Se deberá
    imprimir desde el último elemento del array hasta el primero
    Observación: El alumno deberá crear sus propias funciones para realizar este ejercicio. */
    include 'imprimir_vector.php';

    function recorrer($v,$nro){

      if ($nro>-1) {
        echo " Valor: ".$v[$nro]."<br />";
        recorrer($v,--$nro);
      }
    }
    mt_srand( time());
    for ($i=0; $i <20 ; $i++) {
      $aux= mt_rand( 1,10);
      $vector[]=$aux;
    }
    imprimir_vector($vector);
    recorrer($vector,count($vector)-1);

  ?>
  </div>
  </body>
</html>