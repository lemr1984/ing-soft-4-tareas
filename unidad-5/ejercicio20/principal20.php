<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title>Unidad5-Ejercicio20</title>
  </head>
  
  <body>
  <div class="cuerpo">
    <!--  Leer un archivo agenda.txt (creado por el alumno), este archivo contendrá el nombre y el apellido
    de personas.
    Se debe crear una función PHP que busque un nombre y un apellido en dicho archivo e imprima un
    mensaje si se encontró o no se encontró el nombre y apellido en el archivo. Nombre y apellido son
    variables que se debe introducir el usuario por formulario.
    Observación: El alumno deberá crear sus propias funciones para realizar este ejercicio. -->
    <form role="form" method="POST" action="agenda.php">
      <label for="nombre">Nombre:</label>
      <input type="text" name="nombre">
      <label for="apellido">Apellido:</label>
      <input type="text" name="apellido">
      <button type="submit">Buscar</button>
    </form>
  
  </body>
</html>
