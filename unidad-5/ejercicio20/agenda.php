<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title>Tarea 3 - Ejercicio 20</title>
    <link rel="stylesheet" href="CSS/style.css" type="text/css">
  </head>
  <body>
    
  <div class="cuerpo">
<?php
    /* Leer un archivo agenda.txt (creado por el alumno), este archivo contendrá el nombre y el apellido
    de personas.
    Se debe crear una función PHP que busque un nombre y un apellido en dicho archivo e imprima un
    mensaje si se encontró o no se encontró el nombre y apellido en el archivo. Nombre y apellido son
    variables que se debe introducir el usuario por formulario.
    Observación: El alumno deberá crear sus propias funciones para realizar este ejercicio. */

require "abrir_archivo.php";

function buscar_persona($n,$a,$dir){
  $gestor = abrir_archivo($dir);
  if( $gestor == -1 )
  {
    die("No se puede abrir el archivo, se detuvo el script");
  }
  $agenda = file($dir);
  foreach ($agenda as $key => $value) {
    list($nombre,$apellido) = explode(',',$value);
    $nombre   = trim($nombre);
    $apellido = trim($apellido);
    if( $nombre==$n && $apellido==$a)
      return 1;
    }
    return 0;
}
$nombre   = $_POST['nombre'];
$apellido = $_POST['apellido'];
$dire='agenda.txt';
if($nombre && $apellido){
  $existe = buscar_persona($nombre,$apellido,$dire);
  if($existe==1){
    echo "Se econtró a la persona $nombre $apellido";
  }
  else{
  echo "No se econtró a la persona $nombre $apellido";
  }
}
else{
  echo "Error de Ingreso";
}
?>

  </div>
  </body>
</html>
