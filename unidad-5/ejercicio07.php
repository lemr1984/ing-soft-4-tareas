<!DOCTYPE html>
<html lang="es">
   <head>
      <meta charset="utf-8">
      <title>Unidad5-Ejercicio07</title>
   </head>
   <body>
      <?php
         echo '
         <form method="post">
               <input type="text" name="palabra">
               <input type="submit" value="enviar">
         </form>
         ';
         echo '<br>';
            $palabra_aux = strrev(htmlspecialchars($_POST['palabra']));    
            if ( strcmp(htmlspecialchars($_POST['palabra']), $palabra_aux)==0)
            {
               echo 'La palabra '."{$palabra}".' es un palíndromo';
            }
            else 
            {
               echo 'La palabra '."{$palabra}".' no es un palíndromo';
            }
      ?>
   </body>
</html>