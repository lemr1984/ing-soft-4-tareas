<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<title>Unidad5-Ejercicio2</title>
	</head>
	<body>
		<?php
			print "Version de PHP ID: ".PHP_RELEASE_VERSION;
			echo '<br>';
			print "ID de la version de PHP: ".PHP_VERSION_ID;
			echo '<br>';
			print "Valor maximo de enteros soportado: ".PHP_INT_MAX;	
			echo '<br>';
			print "Tamaño máximo del nombre de un archivo: ".PHP_MAXPATHLEN;
			echo '<br>';
			print "Versión del Sistema Operativo: ".PHP_OS;
			echo '<br>';
			print "Símbolo correcto de 'Fin De Línea' para la plataforma en uso: ".PHP_EOL;
			echo '<br>';
			print "El include path por defecto: ".DEFAULT_INCLUDE_PATH;
		?>
	</body>
</html>
