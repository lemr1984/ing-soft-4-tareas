<html lang = "en">
   
   <head>
      <title>Ejercicio21</title>
   </head>

   <body>
      <h3>Ingrese los siguientes datos</h3>
      <div class = "container">
         <form role="form" method="POST" action="guardar-datos.php">
            <input type = "text" name = "nombreApellido" placeholder = "Nombre y Apellido" required autofocus>
            <br><br>
            <input type = "password" name = "contrasena" placeholder = "contraseña" required>
            <br>
            <p>Seleccione su nacionalidad</p>
            <select name="nacionalidad" id="nacionalidad">
               <option value=""></option>   
               <option value="Paraguaya">Paraguaya</option>
               <option value="Argentina">Argentina</option>
               <option value="Uruguaya">Uruguaya</option>
               <option value="Brasilera">Brasilera</option>
            </select>
            <br>
            <p>Seleccione sus niveles educativos:</p>
            <input type="checkbox" id="primaria" name="primaria" value="primaria">
            <label for="Primaria">Primaria</label><br>
            <input type="checkbox" id="secundaria" name="secundaria" value="secundaria">
            <label for="Secundaria">Secundaria</label><br>
            <input type="checkbox" id="terciaria" name="terciaria" value="terciaria">
            <label for="terciaria">Terciaria</label><br>
            <input type="checkbox" id="universitaria" name="universitaria" value="universitaria">
            <label for="universitaria">Universitaria</label><br>
            <input type="checkbox" id="postgrado" name="postgrado" value="postgrado">
            <label for="postgrado">Postgrado</label><br>
            <p>Seleccione su genero:</p>
            <input type="radio" id="masculino" name="genero" value="masculino">
            <label for="masculino">Masculino</label><br>
            <input type="radio" id="femenino" name="genero" value="femenino">
            <label for="femenino">Femenino</label><br>
            <p>Ingrese una observación si lo desea</p>
            <label for="observacion"></label>
            <textarea id="observacion" name="observacion" rows="4" cols="50"></textarea>
            <br><br>
            <button type = "submit" name = "guardar-datos">Guardar</button>
         </form>
      </div> 
   </body>
</html>