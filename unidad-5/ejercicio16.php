<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title>Unidad5-Ejercicio16</title>
  </head>
  
  <body>
    <div class="cuerpo">
      <?php
          /*  Representar el siguiente árbol binario en PHP y recorrerlo en pre-orden.
                                   3
                                /     \
                              6        4
                            /   \       /  \
                          14      9    900   45
                        /   \    /  \
                      100  30   40  15  */

      $arbol_binario = array(3,6,4,14,9,900,45,100,30,40,15,-1,-1,-1,-1); //de arriba a abajo de izquierda a derecha en orden al nivel, -1 cuando no tiene hijos

      function pre_orden($arbol_binario,$i)
      {
        if($i >= (count($arbol_binario)-1)){
          return;
        }
        if($arbol_binario[$i] != -1){
            echo $arbol_binario[$i]." ";
        }
      pre_orden($arbol_binario, 2*$i+1);
      pre_orden($arbol_binario, 2*$i+2);
      }

      function post_orden($arbol_binario,$i)
      {
         if($i >= (count($arbol_binario)-1)){
            return;
        }
         post_orden($arbol_binario, 2*$i+1);
         post_orden($arbol_binario, 2*$i+2);

         if($arbol_binario[$i] != -1){
            echo $arbol_binario[$i]." ";
        }
      }

      function in_orden($arbol_binario,$i)
      {
         if($i >= (count($arbol_binario)-1)){
            return;
        }
         in_orden($arbol_binario, 2*$i+1);
         if($arbol_binario[$i] != -1 ){
            echo $arbol_binario[$i]." ";
        }
         in_orden($arbol_binario, 2*$i+2);
      }
      echo "Pre Orden: ";
      pre_orden($arbol_binario, 0);
      //echo "<br/>Post Orden: ";
      //post_orden($arbol_binario,0);
      //echo "<br/>In Orden: ";
      //in_orden($arbol_binario,0);
      ?>
    </div>
  </body>
</html>
