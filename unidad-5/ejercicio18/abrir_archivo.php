<?php
  function abrir_archivo($dir)
  {
    if( !file_exists($dir) )
    {
      echo 'No existe el archivo'.$dir.'. VERIFICAR! <br />';
      return -1;
    }
    $gestor = fopen($dir,"r");
    if( !$gestor )
    {
      echo "No se pudo abrir el archivo".$dir." ERROR! <br />";
      return -1;
    }
    return $gestor;
  }
?>