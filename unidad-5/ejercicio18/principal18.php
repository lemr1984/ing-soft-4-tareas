<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title>Unidad5-Ejercicio18</title>
  </head>
  
  <body>
    <div class="cuerpo">
      <?php
          /* Realizar un contador de visitas en PHP utilizando un archivo para mantener la cantidad de visitas a
          la página.
          Observación: El alumno deberá crear sus propias funciones para realizar este ejercicio. */

      require 'abrir_archivo.php';

      function imprimir_visitas()
      {
         $dir      = "contador.txt";
         $gestor = abrir_archivo($dir);

         if( $gestor == -1 )
         {
            echo "No se puede abrir el archivo.";
         }
        else {
          $cant_visitas = (integer)fgets($gestor);
         rewind($gestor);
         $cant_visitas += 1;
         fwrite($gestor, $cant_visitas);
         fclose($gestor);
         echo "Total visitas: $cant_visitas.";
        }
      }

      imprimir_visitas();

      ?>
    </div>
  </body>
</html>
